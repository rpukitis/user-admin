# USER ADMIN APP
![alt text](./readme.gif "App animation")

## Motivation
Be able to run provider mocks in browser environment without hitting network and end to end with network enabled

## Installing
1. git clone https://rpukitis@bitbucket.org/rpukitis/user-admin.git
1. create firebase project
1. [add .env file](#add-.env-file-with-firebase-credentials)
1. [add serviceAccountKey.json file](#add-./src/server/serviceaccountkey.json-with-firebase-credentials)
1. [add cypress.env.json file](#add-cypress.env.json)
1. yarn
1. yarn start
1. http://localhost:3000

## Installing in mock mode
1. git clone https://rpukitis@bitbucket.org/rpukitis/user-admin.git
1. yarn
1. yarn start:mock
1. http://localhost:3000

## Run integration tests
1. install it in mock or complete mode
1. yarn test

## Run integration tests with coverage
1. install it in mock or complete mode
1. yarn test -- --coverage

## Run end to end tests
1. install it in complete mode
1. close servers running on http://localhost:3000
1. yarn cypress:open:serve
1. wait for CRA to compile
1. run all specs in cypress

## Add .env file with firebase credentials

> You can find this in firebase console / project overview / add an app to get started

[Add firebase to your JavaScript project](https://firebase.google.com/docs/web/setup)

```
  REACT_APP_API_KEY=XXXXxxxx
  REACT_APP_AUTH_DOMAIN=xxxxXXXX.firebaseapp.com
  REACT_APP_DATABASE_URL=https://xxxXXXX.firebaseio.com
  REACT_APP_PROJECT_ID=xxxxXXXX
  REACT_APP_STORAGE_BUCKET=xxxxXXXX.appspot.com
  REACT_APP_MESSAGING_SENDER_ID=xxxxXXXX.appspot.com
```

## Add ./src/server/serviceAccountKey.json with firebase credentials

> You can find this in firebase console / project settings / service accounts / generate new private key

[Add Firebase to your app server](https://firebase.google.com/docs/admin/setup)

```json
  {
    "type": "xxxxXXXX",
    "project_id": "xxxxXXXX",
    "private_key_id": "xxxxXXXX",
    "private_key": "xxxxXXXX",
    "client_email": "xxxxXXXX",
    "client_id": "xxxxXXXX",
    "auth_uri": "xxxxXXXX",
    "token_uri": "xxxxXXXX",
    "auth_provider_x509_cert_url": "xxxxXXXX",
    "client_x509_cert_url": "xxxxXXXX"
  }
```

## Add cypress.env.json

> You can create test user in firebase console / authentication / add user

```json
{
  "firebase_email": "xxxxXXXX@xxxxXXXX.com",
  "firebase_password": "xxxxXXXX"
}
```
