import React from 'react'
import Person from '@material-ui/icons/Person'
import { UserList, UserAdd, UserManage, SignIn, SignUp } from './views'
import { useQuery } from 'react-apollo-hooks'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { ThemeProvider, FirebaseProvider, ApolloProvider, ConfigProvider } from './providers'
import { ApolloProvider as ApolloMockProvider, Firebase } from './mock'
import { GET_AUTH } from './gql'
import { Body, Header } from './components'

function PrivateRoute({ component: Component, ...rest }) {
  const { data } = useQuery(GET_AUTH)

  return (
    <Route
      {...rest}
      render={props => data.auth.isLoggedIn ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: '/sign-up', from: props.location }} />
      )}
    />
  )
}

const tabs = [
  [
    { label: 'User list', value: '/user-list' },
    { label: 'User add', value: '/user-add' },
  ],
  [
    { label: 'Sign up', value: '/sign-up' },
    { label: 'Sign in', value: '/sign-in' },
  ],
]

function MainBody({ location: { pathname } }) {
  return (
    <Body tabs={tabs[0]}>
      <Header icon={Person} message="Admin: John Doe" />
      {pathname === '/user-list' ? <UserList /> : <UserAdd />}
    </Body>
  )
}

function LoginBody({ location: { pathname } }) {
  const { data } = useQuery(GET_AUTH)

  if (data.auth.isLoggedIn) {
    return (
      <Redirect to="/user-list" />
    )
  }

  return (
    <Body tabs={tabs[1]}>
      {pathname === '/sign-up' ? <SignUp /> : <SignIn />}
    </Body>
  )
}

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/sign-up" component={LoginBody} />
        <Route path="/sign-in" component={LoginBody} />
        <PrivateRoute path="/user-list" component={MainBody} />
        <PrivateRoute path="/user-add" component={MainBody} />
        <PrivateRoute path="/user-manage/:id" component={UserManage} />
        <Route path="/" render={() => <Redirect to="/user-list" />} />
      </Switch>
    </Router>
  )
}

function DefaultApp() {
  return (
    <ConfigProvider>
      <ThemeProvider>
        <FirebaseProvider>
          <ApolloProvider>
            <App />
          </ApolloProvider>
        </FirebaseProvider>
      </ThemeProvider>
    </ConfigProvider>
  )
}

function MockApp() {
  return (
    <ConfigProvider>
      <ThemeProvider>
        <FirebaseProvider value={new Firebase()}>
          <ApolloMockProvider>
            <App />
          </ApolloMockProvider>
        </FirebaseProvider>
      </ThemeProvider>
    </ConfigProvider>
  )
}

export default process.env.REACT_APP_ENV === 'mock' ? MockApp : DefaultApp
