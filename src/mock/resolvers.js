import faker from 'faker'

export let users = Array.from({ length: 10 }, () => ({
  id: faker.random.uuid(),
  firstName: faker.name.firstName(),
  lastName: faker.name.lastName(),
  phone: faker.phone.phoneNumber(),
  email: faker.internet.email(),
}))

function findBegin(after, users) {
  if (!after) {
    return 0
  }

  const id = atob(after)
  const index = users.findIndex(user => user.id === id)
  return index + 1
}

function findEnd(first, begin, users) {
  if (!first) {
    return users.length + 1
  }

  return first + begin
}

const resolvers = {
  Query: {
    user: (_, { id }) => users.find(user => user.id === id),
    users: (_, { after, first }) => {
      const begin = findBegin(after, users)
      const end = findEnd(first, begin, users)
      const page = users.slice(begin, end)
      const edges = page.map(user => ({
        cursor: btoa(user.id),
        node: user,
      }))

      return {
        edges,
        totalCount: users.length,
        pageInfo: {
          endCursor: edges[edges.length - 1].cursor,
          hasNextPage: begin + first < users.length,
        },
      }
    },
  },
  Mutation: {
    createUser: (_, { user: input }) => {
      const user = { ...input, id: faker.random.uuid() }
      users.push(user)
      return user
    },
    updateUser: (_, { user: input }) => {
      users = users.map(user => {
        if (user.id !== input.id) {
          return user
        }
        return { ...user, ...input }
      })
      return users.find(user => user.id === input.id)
    },
    deleteUser: (_, { id }) => {
      const user = users.find(user => user.id === id)
      users = users.filter(user => user.id !== id)
      return user
    },
  },
}

export default resolvers
