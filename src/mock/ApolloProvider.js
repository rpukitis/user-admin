import React, { useState, useEffect } from 'react'
import ApolloClient  from 'apollo-client'
import { ApolloProvider as Provider } from 'react-apollo-hooks'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { makeExecutableSchema } from 'graphql-tools'
import { SchemaLink } from 'apollo-link-schema'
import typeDefs from './schema'
import typeResolvers from './resolvers'
import { resolvers, getDefaults } from '../providers/ApolloProvider'

export default function ApolloProvider({ children }) {
  const [client, setClient] = useState(null)

  useEffect(() => {
    const cache = new InMemoryCache()
    const schema = makeExecutableSchema({ typeDefs, resolvers: typeResolvers })
    const client = new ApolloClient({
      cache,
      resolvers,
      link: new SchemaLink({ schema }),
    })
    cache.writeData({ data: getDefaults() })
    window.__APOLLO_CLIENT__ = client
    setClient(client)
  }, [])

  if (!client) {
    return null
  }

  return (
    <Provider client={client}>
      {children}
    </Provider>
  )
}
