export default class Firebase {
  createUserWithEmailAndPassword = () => {
    return Promise.resolve({
      user: {
        getIdToken: () => Promise.resolve('__secret__'),
      },
    })
  }

  signInWithEmailAndPassword = () => {
    return Promise.resolve({
      user: {
        getIdToken: () => Promise.resolve('__secret__'),
      },
    })
  }

  signOut = () => {
    return Promise.resolve()
  }

  delete = () => {
    return Promise.resolve()
  }
}
