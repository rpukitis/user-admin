const typeDefs = `
  input CreateUserInput {
    firstName: String
    lastName: String
    phone: String
    email: String
  }
  input UpdateUserInput {
    id: ID!
    firstName: String
    lastName: String
    phone: String
    email: String
  }
  type User {
    id: ID!
    firstName: String
    lastName: String
    phone: String
    email: String
  }
  type Edge {
    cursor: String!
    node: User!
  }
  type PageInfo {
    endCursor: String
    hasNextPage: Boolean!
  }
  type UsersResultCursor {
    edges: [Edge]!
    pageInfo: PageInfo!
    totalCount: Int!
  }
  type Query {
    user(id: ID!): User
    users(after: String, first: Int): UsersResultCursor
  }
  type Mutation {
    createUser(user: CreateUserInput!): User
    updateUser(user: UpdateUserInput!): User
    deleteUser(id: ID!): User
  }
`

export default typeDefs
