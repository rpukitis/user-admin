import gql from 'graphql-tag'

export default gql`
  mutation SetAuth($auth: AuthInput!) {
    setAuth(auth: $auth) @client {
      isLoggedIn
    }
  }
`
