import gql from 'graphql-tag'

export default gql`
  {
    auth @client {
      isLoggedIn
    }
  }
`
