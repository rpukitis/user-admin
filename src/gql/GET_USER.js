import gql from 'graphql-tag'

export default gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      firstName
      lastName
      phone
      email
    }
  }
`
