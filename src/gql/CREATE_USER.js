import gql from 'graphql-tag'

export default gql`
  mutation CreateUser($user: CreateUserInput!) {
    createUser(user: $user) {
      id
      firstName
      lastName
      phone
      email
    }
  }
`
