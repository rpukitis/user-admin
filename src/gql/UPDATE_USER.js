import gql from 'graphql-tag'

export default gql`
  mutation UpdateUser($user: UpdateUserInput!) {
    updateUser(user: $user) {
      id
      firstName
      lastName
      phone
      email
    }
  }
`
