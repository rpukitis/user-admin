import gql from 'graphql-tag'

export default gql`
  {
    users {
      edges {
        cursor
        node {
          id
          firstName
          lastName
          phone
          email
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`
