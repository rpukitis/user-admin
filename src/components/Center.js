import React from 'react'
import { useTheme } from '@material-ui/styles'
import { unstable_useMediaQuery as useMediaQuery } from '@material-ui/core/useMediaQuery'
import { unstable_Box as Box } from '@material-ui/core/Box'

export default function Center({ children }) {
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.up('md'))

  return (
    <Box display="flex" justifyContent="center">
      <Box width={matches ? 1/2 : 1}>
        {children}
      </Box>
    </Box>
  )
}
