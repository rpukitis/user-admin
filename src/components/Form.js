import React from 'react'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import ErrorIcon from '@material-ui/icons/ErrorOutline'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import { unstable_Box as Box } from '@material-ui/core/Box'

export default function Form(props) {
  const { fields, ContainerProps, ItemProps, disabled, error, submitText, onSubmit } = props
  return (
    <form onSubmit={onSubmit}>
      <Grid container spacing={8} direction="column" {...ContainerProps}>
        {fields.map((props, i) => (
          <Grid item key={i} {...ItemProps}>
            <TextField
              fullWidth
              required
              variant="outlined"
              disabled={disabled}
              {...props}
            />
          </Grid>
        ))}
        {error.message &&
          <Grid data-testid="error" item {...ItemProps}>
            <Box display="flex" alignItems="center" color="error.main">
              <Box mr={1}>
                <ErrorIcon />
              </Box>
              <Typography variant="body1" color="inherit">
                {error.message}
              </Typography>
            </Box>
          </Grid>
        }
        <Grid item {...ItemProps}>
          <Button
            data-testid="submit"
            fullWidth
            variant="contained"
            color="primary"
            size="large"
            type="submit"
            disabled={disabled}
          >
            {disabled ? <CircularProgress /> : submitText}
          </Button>
        </Grid>
      </Grid>
    </form>
  )
}

Form.defaultProps = {
  fields: [],
  ContainerProps: {},
  ItemProps: {},
  error: { message: '' },
}
