import React from 'react'
import { withRouter } from 'react-router-dom'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

export default withRouter(({ history, tabs }) => (
  <Tabs
    variant="fullWidth"
    value={history.location.pathname}
    onChange={(_, value) => history.push(value)}
  >
    {tabs.map(({ label, value }) => (
      <Tab
        data-testid={label.toLowerCase().replace(' ', '')}
        key={value}
        label={label}
        value={value}
      />
    ))}
  </Tabs>
))
