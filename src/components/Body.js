import React from 'react'
import Paper from '@material-ui/core/Paper'
import { unstable_Box as Box } from '@material-ui/core/Box'
import { Navigation } from '../components'

export default function Body({ tabs, children }) {
  return (
    <Paper>
      <Navigation tabs={tabs} />
      <Box p={2}>
        {children}
      </Box>
    </Paper>
  )
}
