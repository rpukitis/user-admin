import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import Person from '@material-ui/icons/Person'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles'
import { useMutation } from 'react-apollo-hooks'
import { useFirebase } from '../providers/FirebaseProvider'
import { SET_AUTH } from '../gql'

const useStyles = makeStyles({
  avatar: {
    width: '24px',
    height: '24px',
    marginLeft: '5px',
  },
  icon: {
    fontSize: '16px',
  },
})

export default function SignOut() {
  const classes = useStyles()
  const setAuth = useMutation(SET_AUTH)
  const firebase = useFirebase()

  const handleSignOut = async () => {
    try {
      await firebase.signOut()
      window.localStorage.removeItem('__TOKEN__')
      setAuth({ variables: { auth: { isLoggedIn: false } } })
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Button data-testid="singout" size="small" onClick={handleSignOut}>
      <Typography variant="caption" component="span">
        sign out
      </Typography>
      <Avatar className={classes.avatar}>
        <Person className={classes.icon} />
      </Avatar>
    </Button>
  )
}
