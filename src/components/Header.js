import React from 'react'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Divider from '@material-ui/core/Divider'
import { unstable_Box as Box } from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/styles'
import SingOut from './SignOut'
import DeleteAccount from './DeleteAccount'

const useStyles = makeStyles({
  icon: {
    fontSize: '28px',
    marginRight: '8px',
  },
})

export default function Header({ icon: Icon, message }) {
  const classes = useStyles()

  return (
    <>
      <Grid container>
        <Grid item container alignItems="center" xs={6}>
          <Icon className={classes.icon} />
          <Typography variant="h6" component="h2">
            {message}
          </Typography>
        </Grid>
        <Grid item container xs={6} justify="flex-end" alignItems="center">
          <DeleteAccount />
          <SingOut />
        </Grid>
      </Grid>
      <Box py={2}>
        <Divider className={classes.divider} />
      </Box>
    </>
  )
}
