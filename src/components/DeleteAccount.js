import React, { useState } from 'react'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import DeleteForever from '@material-ui/icons/DeleteForever'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Slide from '@material-ui/core/Slide'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/styles'
import { useMutation } from 'react-apollo-hooks'
import { useFirebase } from '../providers/FirebaseProvider'
import { SET_AUTH } from '../gql'

const useStyles = makeStyles({
  avatar: {
    width: '24px',
    height: '24px',
    marginLeft: '5px',
  },
  icon: {
    fontSize: '16px',
  },
})

function Transition(props) {
  return (
    <Slide direction="up" {...props} />
  )
}

export default function DeleteAccount() {
  const classes = useStyles()
  const setAuth = useMutation(SET_AUTH)
  const [open, setOpen] = useState(false)
  const firebase = useFirebase()

  const handleDeleteAccount = async () => {
    try {
      await firebase.delete()
      window.localStorage.removeItem('__TOKEN__')
      setAuth({ variables: { auth: { isLoggedIn: false } } })
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <>
      <Button data-testid="deleteaccount" size="small" onClick={() => setOpen(true)}>
        <Typography variant="caption" component="span">
          delete
        </Typography>
        <Avatar className={classes.avatar}>
          <DeleteForever className={classes.icon} />
        </Avatar>
      </Button>
      <Dialog
        open={open}
        keepMounted
        TransitionComponent={Transition}
        onClick={() => setOpen(false)}
      >
        <DialogTitle>Delete Admin Account</DialogTitle>
        <DialogContent>
          <DialogContentText>Are you sure you want to delete admin account?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={() => setOpen(false)}>
            No
          </Button>
          <Button
            data-testid="deleteaccountconfirm"
            color="primary"
            onClick={() => handleDeleteAccount()}
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
