import React from 'react'
import { ThemeProvider as Provider } from '@material-ui/styles'
import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true,
  },
})

export default function ThemeProvider({ children }) {
  return (
    <Provider theme={theme}>
      {children}
    </Provider>
  )
}
