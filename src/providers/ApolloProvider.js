import React, { useState, useEffect } from 'react'
import ApolloClient  from 'apollo-boost'
import { ApolloProvider as Provider } from 'react-apollo-hooks'
import { persistCache } from 'apollo-cache-persist'
import { useConfig } from './ConfigProvider'
import { GET_AUTH } from '../gql'

export const getDefaults = () => ({
  auth: {
    isLoggedIn: !!window.localStorage.getItem('__TOKEN__'),
    __typename: 'Auth',
  },
})

export const typeDefs = `
  input AuthInput {
    isLoggedIn: Boolean!
  }

  type Auth {
    isLoggedIn: Boolean
  }

  Query {
    auth: Auth
  }

  Mutation {
    setAuth(auth: AuthInput!): Auth
  }
`

export const resolvers = {
  Mutation: {
    setAuth: (_, { auth }, { cache }) => {
      const query = cache.readQuery({ query: GET_AUTH })
      const data = { auth: { ...query.auth, ...auth } }
      cache.writeData({ data })
      return data.auth
    },
  },
}

export default function ApolloProvider({ children }) {
  const config = useConfig()
  const [client, setClient] = useState(null)

  useEffect(() => {
    const client = new ApolloClient({
      uri: config.uri,
      clientState: { defaults: getDefaults(), typeDefs, resolvers },
      request: (operation) => {
        const token = window.localStorage.getItem('__TOKEN__')
        operation.setContext({headers: {
          authorization: `Bearer ${token}`,
        }})
      },
    })
    persistCache({ cache: client.cache, storage: window.localStorage })
      .then(_ => setClient(client))
  }, [])

  if (!client) {
    return null
  }

  return (
    <Provider client={client}>
      {children}
    </Provider>
  )
}
