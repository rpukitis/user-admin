import React, { useContext } from 'react'
import config from '../config'

const ConfigContext = React.createContext(null)

export function useConfig() {
  return useContext(ConfigContext)
}

export function withConfig(WrappedComponent) {
  return props => (
    <ConfigContext.Consumer>
      {value => <WrappedComponent {...props} config={value} />}
    </ConfigContext.Consumer>
  )
}

export default function ConfigProvider({ value, children }) {
  return (
    <ConfigContext.Provider value={value || config}>
      {children}
    </ConfigContext.Provider>
  )
}
