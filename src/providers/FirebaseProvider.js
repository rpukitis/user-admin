import React, { useContext } from 'react'
import app from 'firebase/app'
import 'firebase/auth'

const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
}

class Firebase {
  constructor() {
    app.initializeApp(config)
    this.auth = app.auth()
  }

  createUserWithEmailAndPassword = (email, password) => {
    return this.auth.createUserWithEmailAndPassword(email, password)
  }

  signInWithEmailAndPassword = (email, password) => {
    return this.auth.signInWithEmailAndPassword(email, password)
  }

  signOut = () => {
    return this.auth.signOut()
  }

  delete = () => {
    return this.auth.currentUser.delete()
  }
}

const FirebaseContext = React.createContext(null)

export function useFirebase() {
  return useContext(FirebaseContext)
}

export function withFirebase(WrappedComponent) {
  return props => (
    <FirebaseContext.Consumer>
      {value => <WrappedComponent {...props} firebase={value} />}
    </FirebaseContext.Consumer>
  )
}

export default function FirebaseProvider({ value, children }) {
  return (
    <FirebaseContext.Provider value={value || new Firebase()}>
      {children}
    </FirebaseContext.Provider>
  )
}
