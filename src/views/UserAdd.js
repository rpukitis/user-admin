import React, { useState, useRef } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { CREATE_USER } from '../gql'
import { Form } from '../components'

export default function UserAdd() {
  const [disabled, setDisabled] = useState(false)
  const firsName = useRef()
  const lastName = useRef()
  const phone = useRef()
  const email = useRef()
  const createUser = useMutation(CREATE_USER, {
    update: () => {
      setDisabled(false)
      const refs = [firsName, lastName, phone, email]
      refs.forEach(ref => ref.current.value = '')
    },
  })

  const handleSubmit = (e) => {
    e.preventDefault()
    setDisabled(true)
    createUser({
      variables: {
        user: {
          firstName: firsName.current.value,
          lastName: lastName.current.value,
          phone: phone.current.value,
          email: email.current.value,
        },
      },
    })
  }

  const fields = [
    { id:'firsName', label: 'first name', inputRef: firsName },
    { id:'lastName', label: 'last name', inputRef: lastName },
    { id:'email', label: 'email', inputRef: email },
    { id:'phone', label: 'phone', inputRef: phone },
  ]

  return (
    <Form
      ItemProps={{ xs: 12, md: 6 }}
      fields={fields}
      disabled={disabled}
      submitText="add user"
      onSubmit={handleSubmit}
    />
  )
}
