import React from 'react'
import NavigateNextIcon from '@material-ui/icons/NavigateNext'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItem from '@material-ui/core/ListItem'
import List from '@material-ui/core/List'
import { useQuery } from 'react-apollo-hooks'
import { Link } from 'react-router-dom'
import { GET_USERS } from '../gql'

export default function UserList() {
  const { data, error, loading } = useQuery(GET_USERS, {
    fetchPolicy: 'network-only',
  })

  if (loading) {
    return <div>loading...</div>
  }

  if (error && !data.users) {
    return <div>error...</div>
  }

  return (
    <List>
      {data.users.edges.map(({ node }, index) => (
        <Link
          key={node.id}
          to={`/user-manage/${node.id}`}
          style={{ textDecoration: 'none', width: '100%', height: '100%' }}
        >
          <ListItem button data-testid='useritem'>
            <ListItemText
              primary={`${node.firstName} ${node.lastName}`}
              secondary={node.email}
            />
            <ListItemIcon>
              <NavigateNextIcon />
            </ListItemIcon>
          </ListItem>
        </Link>
      ))}
    </List>
  )
}
