import React from 'react'
import { render, waitForElement, fireEvent, wait } from 'react-testing-library'
import App from '../../App'

beforeEach(() => {
  window.localStorage.setItem('__TOKEN__', '__secret__')
})

afterEach(() => {
  window.localStorage.removeItem('__TOKEN__')
})

test('navigates to user add section and adds new user', async () => {
  const { getByTestId, getByLabelText, getByText, debug } = render(<App />)
  const userAdd = await waitForElement(() => getByTestId('useradd'))
  fireEvent.click(userAdd)

  const submit = await waitForElement(() => getByTestId('submit'))
  const userList = getByTestId('userlist')
  const firstName = getByLabelText(/first name/i)
  const lastName = getByLabelText(/last name/i)
  const email = getByLabelText(/email/i)
  const phone = getByLabelText(/phone/i)

  fireEvent.change(firstName, {
    target: { value: 'John' },
  })

  fireEvent.change(lastName, {
    target: { value: 'Doe' },
  })

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(phone, {
    target: { value: '22123456' },
  })

  fireEvent.click(submit)
  fireEvent.click(userList)

  await waitForElement(() => getByText(/john.doe@mail.com/i))
})
