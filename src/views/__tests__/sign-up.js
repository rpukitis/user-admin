import React from 'react'
import { render, fireEvent, wait } from 'react-testing-library'
import App from '../../App'

beforeEach(() => {
  window.localStorage.removeItem('__TOKEN__')
})

function renderSignUpForm() {
  const utils = render(<App />)
  const email = utils.getByLabelText(/email/i)
  const password = utils.getByLabelText(/password/i)
  const confirm = utils.getByLabelText(/confirm/i)
  const submit = utils.getByTestId('submit')
  return { utils, email, password, confirm, submit }
}

test('Fills and submits sign up form', async () => {
  const { utils, email, password, confirm, submit } = renderSignUpForm()

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(password, {
    target: { value: 'secret123' },
  })

  fireEvent.change(confirm, {
    target: { value: 'secret123' },
  })

  fireEvent.click(submit)
  await wait(() => expect(submit).not.toBeInTheDocument())
  const signOut = utils.getByTestId('singout')
  expect(signOut).toBeInTheDocument()
})

test('Misspells confirm password field and fails to submit sign up form', async () => {
  const { utils, email, password, confirm, submit } = renderSignUpForm()

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(password, {
    target: { value: 'secret123' },
  })

  fireEvent.change(confirm, {
    target: { value: 'secret124' },
  })

  fireEvent.click(submit)
  const error = utils.getByTestId('error')
  expect(error).toBeInTheDocument()
})
