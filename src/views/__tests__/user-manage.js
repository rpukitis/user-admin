import React from 'react'
import { render, waitForElement, wait, fireEvent } from 'react-testing-library'
import App from '../../App'
import { users } from '../../mock/resolvers'

beforeEach(() => {
  window.localStorage.setItem('__TOKEN__', '__secret__')
})

afterEach(() => {
  window.localStorage.removeItem('__TOKEN__')
})

test('clicks user link and updates data', async () => {
  const { getByTestId, getByLabelText, getByText } = render(<App />)
  const userItem = await waitForElement(() => getByTestId('useritem'))
  fireEvent.click(userItem)

  const submit = await waitForElement(() => getByTestId('submit'))
  const firstName = getByLabelText(/first name/i)
  const lastName = getByLabelText(/last name/i)
  const email = getByLabelText(/email/i)
  const phone = getByLabelText(/phone/i)
  const back = getByTestId('back')

  fireEvent.change(firstName, {
    target: { value: 'John' },
  })

  fireEvent.change(lastName, {
    target: { value: 'Doe' },
  })

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(phone, {
    target: { value: '22123456' },
  })

  fireEvent.click(submit)
  fireEvent.click(back)

  await wait(() => waitForElement(() => getByText(/john.doe@mail.com/i)))
})

test('clicks user link and deletes data', async () => {
  const { getByTestId, queryByText } = render(<App />)

  const userEmail = users[0].email
  const userItem = await waitForElement(() => getByTestId('useritem'))
  fireEvent.click(userItem)

  const deleteButton = await waitForElement(() => getByTestId('delete'))
  fireEvent.click(deleteButton)

  const deleteConfirm = getByTestId('confirm')
  fireEvent.click(deleteConfirm)

  await waitForElement(() => getByTestId('useritem'))
  expect(queryByText(userEmail)).not.toBeInTheDocument()
})
