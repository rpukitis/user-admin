import React from 'react'
import { render, fireEvent, wait } from 'react-testing-library'
import App from '../../App'

beforeEach(() => {
  window.localStorage.removeItem('__TOKEN__')
})

function renderSignInForm() {
  const utils = render(<App />)
  const tab = utils.getByTestId('signin')
  fireEvent.click(tab)

  const email = utils.getByLabelText(/email/i)
  const password = utils.getByLabelText(/password/i)
  const submit = utils.getByTestId('submit')
  return { utils, email, password, submit }
}

test('logs in and then logs out', async () => {
  const { utils, email, password, submit } = renderSignInForm()

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(password, {
    target: { value: 'secret123' },
  })

  fireEvent.click(submit)
  await wait(() => expect(submit).not.toBeInTheDocument())
  const signOut = utils.getByTestId('singout')
  fireEvent.click(signOut)
  await wait(() => expect(signOut).not.toBeInTheDocument())
  expect(window.location.pathname).toEqual('/sign-up')
})

test('logs in and then deletes admin account', async () => {
  const { utils, email, password, submit } = renderSignInForm()

  fireEvent.change(email, {
    target: { value: 'john.doe@mail.com' },
  })

  fireEvent.change(password, {
    target: { value: 'secret123' },
  })

  fireEvent.click(submit)
  await wait(() => expect(submit).not.toBeInTheDocument())
  const deleteAccount = utils.getByTestId('deleteaccount')
  fireEvent.click(deleteAccount)
  const deleteAccountConfirm = utils.getByTestId('deleteaccountconfirm')
  fireEvent.click(deleteAccountConfirm)
  await wait(() => expect(deleteAccount).not.toBeInTheDocument())
  expect(window.location.pathname).toEqual('/sign-up')
})
