import React from 'react'
import { render, waitForElement, getByText, fireEvent } from 'react-testing-library'
import App from '../../App'
import { users } from '../../mock/resolvers'

beforeEach(() => {
  window.localStorage.setItem('__TOKEN__', '__secret__')
})

afterEach(() => {
  window.localStorage.removeItem('__TOKEN__')
})

test('shows user link list', async () => {
  const { getAllByTestId } = render(<App />)
  const userItems = await waitForElement(() => getAllByTestId('useritem'))
  userItems.forEach((item, index) => {
    getByText(item, users[index].email)
  })
})

test('clicks user link', async () => {
  const { getByTestId } = render(<App />)
  const userItem = await waitForElement(() => getByTestId('useritem'))
  fireEvent.click(userItem)

  expect(window.location.pathname).toEqual(`/user-manage/${users[0].id}`)
})
