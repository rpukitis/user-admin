import React, { useRef, useState } from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import TextField from '@material-ui/core/TextField'
import Slide from '@material-ui/core/Slide'
import Person from '@material-ui/icons/Person'
import Paper from '@material-ui/core/Paper'
import NavigateBefore from '@material-ui/icons/NavigateBefore'
import IconButton from '@material-ui/core/IconButton'
import Grid from '@material-ui/core/Grid'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'
import CircularProgress from '@material-ui/core/CircularProgress'
import Button from '@material-ui/core/Button'
import AppBar from '@material-ui/core/AppBar'
import { withRouter } from 'react-router-dom'
import { useQuery, useMutation } from 'react-apollo-hooks'
import { unstable_Box as Box } from '@material-ui/core/Box'
import { Header } from '../components'
import { GET_USER, UPDATE_USER, DELETE_USER } from '../gql'

function Transition(props) {
  return (
    <Slide direction="up" {...props} />
  )
}

function Body({ history, children }) {
  return (
    <Paper>
      <AppBar position="static" color="primary">
        <Toolbar variant="dense">
          <IconButton
            data-testid="back"
            color="inherit"
            onClick={() => history.goBack()}
          >
            <NavigateBefore />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Box p={2}>
        <Header icon={Person} message="Admin: John Doe" />
        {children}
      </Box>
    </Paper>
  )
}

function UserManage({ history, match: { params: { id } } }) {
  const [disabled, setDisabled] = useState(false)
  const [open, setOpen] = useState(false)
  const firsName = useRef()
  const lastName = useRef()
  const phone = useRef()
  const email = useRef()

  const { data, error, loading } = useQuery(GET_USER, {
    variables: { id },
  })

  const updateUser = useMutation(UPDATE_USER, {
    update: () => {
      setDisabled(false)
    },
  })

  const deleteUser = useMutation(DELETE_USER, {
    variables: { id },
    update: () => {
      history.goBack()
    },
  })

  const handleUpdate = () => {
    setDisabled(true)
    updateUser({
      variables: {
        user: {
          id,
          firstName: firsName.current.value,
          lastName: lastName.current.value,
          phone: phone.current.value,
          email: email.current.value,
        },
      },
    })
  }

  if (loading) {
    return (
      <Body history={history}>
        <div>loading...</div>
      </Body>
    )
  }

  if (error) {
    return (
      <Body history={history}>
        <div>error...</div>
      </Body>
    )
  }

  return (
    <>
      <Body history={history}>
        <form>
          <Grid container spacing={16} direction="column">
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                required
                id="firstName"
                variant="outlined"
                inputRef={firsName}
                label="first name"
                defaultValue={data.user.firstName}
                disabled={disabled}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                required
                id="lastName"
                variant="outlined"
                inputRef={lastName}
                label="last name"
                defaultValue={data.user.lastName}
                disabled={disabled}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                required
                id="email"
                variant="outlined"
                inputRef={email}
                label="email"
                defaultValue={data.user.email}
                disabled={disabled}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <TextField
                fullWidth
                required
                id="phone"
                variant="outlined"
                inputRef={phone}
                label="phone"
                defaultValue={data.user.phone}
                disabled={disabled}
              />
            </Grid>
          </Grid>
          <Grid container spacing={16}>
            <Grid item xs={12} md={6}>
              <Button
                fullWidth
                data-testid="delete"
                variant="contained"
                color="primary"
                size="large"
                disabled={disabled}
                onClick={() => setOpen(true)}
              >
                {disabled ? <CircularProgress /> : 'Delete'}
              </Button >
            </Grid>
            <Grid item xs={12} md={6}>
              <Button
                fullWidth
                data-testid="submit"
                variant="contained"
                color="primary"
                size="large"
                disabled={disabled}
                onClick={handleUpdate}
              >
                {disabled ? <CircularProgress /> : 'Update'}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Body>
      <Dialog
        open={open}
        keepMounted
        TransitionComponent={Transition}
        onClick={() => setOpen(false)}
      >
        <DialogTitle>Delete User</DialogTitle>
        <DialogContent>
          <DialogContentText>Are you sure you want to delete user?</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={() => setOpen(false)}>
            No
          </Button>
          <Button
            data-testid="confirm"
            color="primary"
            onClick={() => deleteUser()}
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default withRouter(UserManage)
