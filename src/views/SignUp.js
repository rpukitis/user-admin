import React, { useState, useRef } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { Center, Form } from '../components'
import { useFirebase } from '../providers/FirebaseProvider'
import { SET_AUTH } from '../gql'

export default function SignUp() {
  const [disabled, setDisabled] = useState(false)
  const [error, setError] = useState({ message: '' })
  const email = useRef()
  const password1 = useRef()
  const password2 = useRef()
  const firebase = useFirebase()
  const setAuth = useMutation(SET_AUTH)

  const handleSubmit = async (e) => {
    e.preventDefault()

    if (password1.current.value !== password2.current.value) {
      password1.current.value = password2.current.value = ''
      setError({ message: 'Password fields don\'t match, please try again!' })
      return
    }

    setError({ message: '' })
    setDisabled(true)

    try {
      const auth = await firebase.createUserWithEmailAndPassword(
        email.current.value,
        password1.current.value,
      )
      const token = await auth.user.getIdToken()
      window.localStorage.setItem('__TOKEN__', token)
      setAuth({ variables: { auth: { isLoggedIn: true } } })
    } catch(error) {
      setError(error)
      setDisabled(false)
    }
  }

  const fields = [
    { id: 'email', label: 'email', type: 'email', inputRef: email },
    { id: 'password', label: 'password', type: 'password', inputRef: password1 },
    { id: 'confirm', label: 'confirm password', type: 'password', inputRef: password2 },
  ]

  return (
    <Center>
      <Form
        fields={fields}
        disabled={disabled}
        error={error}
        submitText="sign up"
        onSubmit={handleSubmit}
      />
    </Center>
  )
}
