import React, { useState, useRef } from 'react'
import { useMutation } from 'react-apollo-hooks'
import { SET_AUTH } from '../gql'
import { useFirebase } from '../providers/FirebaseProvider'
import { Center, Form } from '../components'

export default function SignIn() {
  const [disabled, setDisabled] = useState(false)
  const [error, setError] = useState({ message: '' })
  const email = useRef()
  const password = useRef()
  const firebase = useFirebase()
  const setAuth = useMutation(SET_AUTH)

  const handleSubmit = async (e) => {
    e.preventDefault()
    setError({ message: '' })
    setDisabled(true)

    try {
      const auth = await firebase.signInWithEmailAndPassword(
        email.current.value,
        password.current.value,
      )
      const token = await auth.user.getIdToken()
      window.localStorage.setItem('__TOKEN__', token)
      setAuth({ variables: { auth: { isLoggedIn: true } } })
    } catch(error) {
      setError(error)
      setDisabled(false)
    }
  }

  const fields = [
    { id: 'email', label: 'email', type: 'email', inputRef: email },
    { id: 'password', label: 'password', type: 'password', inputRef: password },
  ]

  return (
    <Center>
      <Form
        fields={fields}
        disabled={disabled}
        error={error}
        submitText="sign in"
        onSubmit={handleSubmit}
      />
    </Center>
  )
}
