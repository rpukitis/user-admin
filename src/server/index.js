const { ApolloServer } = require('apollo-server')
const admin = require('firebase-admin')
const account = require('./serviceAccountKey.json')
const typeDefs = require('./schema')
const resolvers = require('./resolvers')

let cache = {}

admin.initializeApp({ credential: admin.credential.cert(account) })

function context({ req }) {
  const token = req.headers.authorization.replace('Bearer ', '')

  if (cache[token]) {
    return {}
  }

  return new Promise((resolve, reject) => {
    admin.auth().verifyIdToken(token)
      .then(_ => {
        cache[token] = token
        resolve({})
      })
      .catch(error => {
        reject(error)
      })
  })
}

const server = new ApolloServer({ typeDefs, resolvers, context })
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`)
})
