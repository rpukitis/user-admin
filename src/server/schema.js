const { gql } = require('apollo-server')

const client = `
  firstName: String
  lastName: String
  phone: String
  email: String
`
const typeDefs = gql`
  input CreateUserInput {
    ${client}
  }
  input UpdateUserInput {
    id: ID!
    ${client}
  }
  type User {
    id: ID!
    ${client}
  }
  type Edge {
    cursor: String!
    node: User!
  }
  type PageInfo {
    endCursor: String
    hasNextPage: Boolean!
  }
  type UsersResultCursor {
    edges: [Edge]!
    pageInfo: PageInfo!
    totalCount: Int!
  }
  type Query {
    user(id: ID!): User
    users(after: String, first: Int): UsersResultCursor
  }
  type Mutation {
    createUser(user: CreateUserInput!): User
    updateUser(user: UpdateUserInput!): User
    deleteUser(id: ID!): User
  }
`

module.exports = typeDefs
