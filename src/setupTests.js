import './bootstrap'
import 'jest-dom/extend-expect'
import 'react-testing-library/cleanup-after-each'
window.matchMedia = jest.fn(() => ({ matches: true, addListener: () => null, removeListener: () => null }))